var ObjetoParrafo = {
  parrafoUno : function () {
    document.getElementById('myDIV').innerHTML = '';
    var titulo = document.createElement("H4");
    var txto_titulo = document.createTextNode("Servicio de Transporte");
    titulo.appendChild(txto_titulo);
    document.getElementById("myDIV").appendChild(titulo);

    var para = document.createElement("H5");
    var t = document.createTextNode("Nace en Octubre del 2006 ante la necesidad de la empresa en enfocarse en su negocio y seguir prestando un servicio de calidad para sus colaboradores. Actualmente con una flotilla de 7 camionetas TOYOTA HIACE de modelos recientes y con una planilla de 13 colaboradores que prestan su labor las 24 horas del día los 365 días del año. En ASETACA nos hemos consolidado con un servicio de calidad trasladando personal de la operación nocturna así como tripulaciones..");
    para.appendChild(t);
    document.getElementById("myDIV").appendChild(para);

  },
  parrafoDos : function(){
    document.getElementById('myDIV').innerHTML = '';
    var titulo = document.createElement("H4");
    var txto_titulo = document.createTextNode("Servicios Misceláneos de Limpieza");
    titulo.appendChild(txto_titulo);
    document.getElementById("myDIV").appendChild(titulo);

    var para = document.createElement("H5");
    var t = document.createTextNode("Se inicia en el año 2009, brindando el servicio para todas las áreas de la empresa en el Aeropuerto Juan Santamaría y TACA Centers. Pocos meses después ya prestábamos servicios a otras aerolíneas en sus oficinas del Aeropuerto. En el año 2012 nuestro servicio incluyó las oficinas de la empresa en la Uruca. Podemos estar orgullosos que al día de hoy este servicio se ha extendido a toda la Empresa en Costa Rica.");
    para.appendChild(t);
    document.getElementById("myDIV").appendChild(para);
  },
  parrafoTres : function() {
    document.getElementById('myDIV').innerHTML = '';
    var titulo = document.createElement("H4");
    var txto_titulo = document.createTextNode("Sansa café");
    titulo.appendChild(txto_titulo);
    document.getElementById("myDIV").appendChild(titulo);

    var para = document.createElement("H5");
    var t = document.createTextNode("En el año 2011 ASETACA adquirió la administración de la cafetería ubicada en la sala de abordaje de los pasajeros de SANSA en el Aeropuerto Juan Santamaría. Esta funciona como una pequeña tienda de conveniencia para los pasajeros que viajan a diferentes destinos de Costa Rica por medio de Sansa, ofreciéndoles refrescos, café, jugos, helados, galletas y otras golosinas.");
    para.appendChild(t);
    document.getElementById("myDIV").appendChild(para);
  }
};


